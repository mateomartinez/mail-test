<?php


namespace Tests\Unit\Abstracts;


use GuzzleHttp\Client;
use PHPUnit_Framework_TestCase;

class EmailTestCase extends \Codeception\Test\Unit
{

    /**
     * @var Client
     */
    private $mailcatcher;

    protected function setUp(): void
    {
        $this->mailcatcher = new Client(['base_uri' => 'http://172.19.0.5:1080']);

        // clean emails between tests
        //$this->cleanMessages();
    }

    // api calls
    protected function cleanMessages()
    {
        $this->mailcatcher->delete('/messages');
    }

    protected function getLastMessage()
    {
        $messages = $this->getMessages();
        if (empty($messages)) {
            $this->fail("No messages received");
        }
        // messages are in descending order
        return reset($messages);
    }

    protected function getMessages()
    {
        $jsonResponse = $this->mailcatcher->get('/messages');
        return json_decode($jsonResponse->getBody());
    }

    // assertions
    protected function assertEmailIsSent($description = '')
    {
        $this->assertNotEmpty($this->getMessages(), $description);
    }

    protected function assertEmailSubjectContains($needle, $email, $description = '')
    {
        $this->assertContains($needle, $email->subject, $description);
    }

    protected function assertEmailSubjectEquals($expected, $email, $description = '')
    {
        $this->assertContains($expected, $email->subject, $description);
    }

    protected function assertEmailHtmlContains($needle, $email, $description = '')
    {
        $response = $this->mailcatcher->get("/messages/{$email->id}.html");
        $this->assertStringContainsString($needle, (string)$response->getBody(), $description);
    }

    protected function assertEmailTextContains($needle, $email, $description = '')
    {
        $response = $this->mailcatcher->get("/messages/{$email->id}.plain");
        $this->assertStringContainsString($needle, (string)$response->getBody(), $description);
    }

    protected function assertEmailSenderEquals($expected, $email, $description = '')
    {
        $response = $this->mailcatcher->get("/messages/{$email->id}.json");
        $email = json_decode($response->getBody());
        $this->assertEquals($expected, $email->sender, $description);
    }

    protected function assertEmailRecipientsContain($needle, $email, $description = '')
    {
        $response = $this->mailcatcher->get("/messages/{$email->id}.json");
        $email = json_decode($response->getBody());
        $this->assertContains($needle, $email->recipients, $description);
    }
}
