<?php

namespace Tests\Unit;

use App\Http\Mail\ConfirmationCode;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use Tests\Unit\Abstracts\EmailTestCase;

class MailTest extends EmailTestCase
{
    public function testEmail()
    {
        $email = $this->getLastMessage();
        $this->assertEmailHtmlContains('Hello name', $email);
        $this->assertEmailHtmlContains('Here is your confirmation code: <strong>1234</strong>', $email);
    }
}
