{{-- resources/emails/confirmation-code.blade.php --}}
Hello {{ $user }}

Here is your confirmation code: <strong>{{ $code }}</strong>
