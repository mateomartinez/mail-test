<?php


namespace App\Http\Controllers\Mail;


use App\Http\Controllers\Controller;
use App\Http\Mail\ConfirmationCode;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    public function index()
    {
        $subject = new ConfirmationCode('name', '1234');
        $subject->from('test1@test.com');
        Mail::to('test@test.com')->send($subject);
        return 'ok';
    }
}
